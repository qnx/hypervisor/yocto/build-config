This repo supports Yocto build configurations for generating QAVF guest images.  It should be checked out into $OEROOT/build/conf in the Yocto workspace (see oe-init-build-env).
